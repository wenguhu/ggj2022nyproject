using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementEffect : MonoBehaviour
{
	[SerializeField]
	LocalReference reference;
	Camera mainCamera;
	RectTransform canvasRect;
	public Vector2 ScreenToCanvasPoint(Vector2 object2DPosition)
	{
		//Mouse position to Canvas Pos
		//if(mainCamera == null) Init();
		Vector2 ViewportPosition = mainCamera.ScreenToViewportPoint(object2DPosition);
		Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);

		//Debug: Canvas scaler should be 'Match Height' mode
		Vector2 uiOffset = new Vector2(canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);
		// Set the position and remove the screen offset
		return proportionalPosition - uiOffset;
	}
	// Use this for initialization
	void Start()
	{
		mainCamera = reference.GetReference<Transform>("MainCamera").GetComponent<Camera>();
		canvasRect = GetComponent<RectTransform>();
	}
	// Update is called once per frame
	void Update()
	{
		mainCamera.transform.position = -0.0003f * ScreenToCanvasPoint(Input.mousePosition);

	}
}
