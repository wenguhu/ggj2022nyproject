﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reference_GameManager : Reference<GameManager> {

	private Reference_GameManager()
    {
        referenceName = "GameManager";
    }

    protected override void Awake()
    {
        reference = GetComponent<GameManager>();
        base.Awake();
    }
}
