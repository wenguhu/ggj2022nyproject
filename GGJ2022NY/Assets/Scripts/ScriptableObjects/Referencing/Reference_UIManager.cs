﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reference_UIManager : Reference<UIManager> {

	private Reference_UIManager()
    {
        referenceName = "Reference";
    }

    protected override void Awake()
    {
        reference = GetComponent<UIManager>();
        base.Awake();
    }
}
