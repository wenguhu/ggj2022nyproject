using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackMaskInGame : MonoBehaviour
{
	[SerializeField]
	UIManager uiManager;
	[SerializeField]
	Animator anim;

	private void Start()
	{
		uiManager.onStartSwitchScene += StartSwitchScene;
	}

	private void OnDestroy()
	{
		uiManager.onStartSwitchScene -= StartSwitchScene;
	}

	public void StartSwitchScene()
	{
		anim.SetTrigger("SwitchScene");
	}

	public void SwitchSceneInAnimation()
	{
		uiManager.SwitchSceneInAnimation();
	}
}
