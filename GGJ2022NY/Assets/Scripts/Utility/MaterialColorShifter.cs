﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialColorShifter : MonoBehaviour
{
	[SerializeField]
	Renderer[] renderers;
	[SerializeField]
	Gradient gradient;
	[SerializeField]
	float shiftSpeed = 0.1f;

	float currentShiftPointer = 0;

	// Update is called once per frame
	void Update()
	{
		currentShiftPointer += Time.deltaTime * shiftSpeed;

		foreach (Renderer r in renderers)
		{
			r.material.color = gradient.Evaluate(currentShiftPointer);
		}
	}
}
