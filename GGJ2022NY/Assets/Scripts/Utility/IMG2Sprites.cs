﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class IMG2Sprites
{
	//Results from LoadAndCropAllFilesToSprites()
	public static List<Sprite> returnResults_Sprites = new List<Sprite>();
	//Crop & return all sprites in the folder
	public static IEnumerator LoadAndCropAllFilesToSprites(string FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight, float CropX = 512, float CropY = 512)
	{
		returnResults_Sprites.Clear();
		List<Sprite> NewSprites = new List<Sprite>();
		var info = new DirectoryInfo(FilePath);
		if (info.Exists)
		{
			var fileInfo = info.GetFiles();

			foreach (FileInfo fileExactPath in fileInfo)
			{
				if (fileExactPath.Extension != ".meta")
				{
					//Debug: Do not use ToString() to convert the path that contains StreamingAssets folder 

					//Debug: Can't nest Coroutines here, therefore this is the IEnumerator version of LoadNewSprites()
					List<Sprite> tempSprites = new List<Sprite>();
					Texture2D SpriteTexture = LoadTexture(fileExactPath.FullName);

					if (SpriteTexture != null)
					{
						int ReadXCount = Mathf.FloorToInt(SpriteTexture.width / CropX);
						int ReadYCount = Mathf.FloorToInt(SpriteTexture.height / CropY);
						if (ReadXCount > 0 && ReadYCount > 0)
						{
							for (int ReadY = ReadYCount - 1; ReadY >= 0; ReadY--)
							{
								for (int ReadX = 0; ReadX < ReadXCount; ReadX++)
								{
									tempSprites.Add(Sprite.Create(SpriteTexture, new Rect(ReadX * CropX, ReadY * CropY, CropX, CropY), new Vector2(0.5f, 0.5f), PixelsPerUnit, 0, spriteType));
									yield return new WaitForEndOfFrame();
								}
							}
						}
					}
					//End
						
					if (tempSprites.Count > 0)
					{
						foreach (Sprite spriteElement in tempSprites)
						{
							NewSprites.Add(spriteElement);
						}
						//e.g. After loading a character clothes
						//Wait for a frame
						yield return new WaitForEndOfFrame();
					}
				}
			}
		}
		returnResults_Sprites = NewSprites;
		yield return "Crop all Sprites";
		//return NewSprites;
	}

	//Return all sprites in the folder without croppiing
	public static List<Sprite> LoadAllFilesToSprites(string FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight)
	{
		List<Sprite> NewSprites = new List<Sprite>();
		var info = new DirectoryInfo(FilePath);
		if (info.Exists)
		{
			var fileInfo = info.GetFiles();

			foreach (FileInfo fileExactPath in fileInfo)
			{
				if (fileExactPath.Extension != ".meta")
				{
					//Debug: Do not use ToString() to convert the path that contains StreamingAssets folder 
					Sprite tempSprite = LoadNewSprite(fileExactPath.FullName, PixelsPerUnit, spriteType);
					if (tempSprite != null)
					{
						NewSprites.Add(tempSprite);
					}
				}
			}
		}
		return NewSprites;
	}


	//Return all textures in the folder
	public static List<Texture> LoadAllFilesToTextures(string FilePath)
	{
		List<Texture> NewTextures = new List<Texture>();
		var info = new DirectoryInfo(FilePath);
		if (info.Exists)
		{
			var fileInfo = info.GetFiles();

			foreach (FileInfo fileExactPath in fileInfo)
			{
				if (fileExactPath.Extension != ".meta")
				{
					//Debug: Do not use ToString() to convert the path that contains StreamingAssets folder 
					Texture tempTexture = LoadTexture(fileExactPath.FullName);
					if (tempTexture != null)
					{
						NewTextures.Add(tempTexture);
					}
				}
			}
		}
		return NewTextures;
	}


	static Texture2D LoadTexture(string FilePath)
	{
		Texture2D Tex2D;
		byte[] FileData;

		if (File.Exists(FilePath))
		{
			FileData = File.ReadAllBytes(FilePath);
			Tex2D = new Texture2D(2, 2);

			if (Tex2D.LoadImage(FileData))
				return Tex2D;
		}

		return null;
	}


	public static Sprite LoadNewSprite(string FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight, float CropX = 9999, float CropY = 9999)
	{
		Sprite NewSprite = null;
		Texture2D SpriteTexture = LoadTexture(FilePath);
		if (CropX == 9999)
		{
			CropX = SpriteTexture.width;
			CropY = SpriteTexture.height;
		}
		NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, CropX, CropY), new Vector2(0, 0), PixelsPerUnit, 0, spriteType);

		return NewSprite;
	}

	//TODO: May not be used anymore, delete?
	static List<Sprite> LoadNewSprites(string FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight, float CropX = 512, float CropY = 512)
	{
		List<Sprite> NewSprites = new List<Sprite>();
		Texture2D SpriteTexture = LoadTexture(FilePath);

		if (SpriteTexture != null)
		{
			int ReadXCount = Mathf.FloorToInt(SpriteTexture.width / CropX);
			int ReadYCount = Mathf.FloorToInt(SpriteTexture.height / CropY);
			if (ReadXCount > 0 && ReadYCount > 0)
			{
				for (int ReadY = ReadYCount - 1; ReadY >= 0; ReadY--)
				{
					for (int ReadX = 0; ReadX < ReadXCount; ReadX++)
					{
						NewSprites.Add(Sprite.Create(SpriteTexture, new Rect(ReadX * CropX, ReadY * CropY, CropX, CropY), new Vector2(0.5f, 0.5f), PixelsPerUnit, 0, spriteType));
					}
				}
			}
		}

		return NewSprites;
	}

}
