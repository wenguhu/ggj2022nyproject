﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LT_ShowUpAnimationWithEnd : MonoBehaviour {

	[SerializeField]
	private Vector3 startScale = new Vector3(0, 0, 0);
	[SerializeField]
	private Vector3 finalScale = new Vector3(1, 1, 1);
	[SerializeField]
	private float transitionTime = 0.5f;
	[SerializeField]
	private float transitionTime_End = 1;

	public bool endAnimationTrigger = false;

	float endTimer = 0;
	float endTimerTotal = 0.25f;

	// Use this for initialization
	void OnEnable ()
	{
		transform.localScale = startScale;
		//StartAnimation();
	}

	void Update()
	{
		if (endAnimationTrigger)
		{
			float currentDeltaTime = Time.deltaTime / Time.timeScale;
			transform.localScale -= 4 / transitionTime_End * new Vector3(currentDeltaTime, currentDeltaTime, currentDeltaTime);
			if (endTimer > endTimerTotal || transform.localScale.x < 0)
			{
				endTimer = 0;
				endAnimationTrigger = false;
				gameObject.SetActive(false);
			}
			else
			{
				endTimer += Time.deltaTime;
			}
		}
	}
	

	// Update is called once per frame
	public void StartAnimation() {
		LeanTween.scale(gameObject, finalScale, transitionTime * Time.timeScale).setEase(LeanTweenType.easeOutBack);
		endTimer = 0;
		endAnimationTrigger = false;
	}

	// Update is called once per frame
	public void EndAnimation()
	{
		endAnimationTrigger = true;
		LeanTween.cancel(gameObject);
	}
	
}
