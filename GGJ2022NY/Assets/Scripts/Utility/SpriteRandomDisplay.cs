﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRandomDisplay : MonoBehaviour {
	[SerializeField]
	SpriteRenderer spriteRenderer;
	[SerializeField]
	Sprite[] randomSprites;
	// Use this for initialization
	void Start () {
		spriteRenderer.sprite = randomSprites[Random.Range(0, randomSprites.Length)];
	}
}
