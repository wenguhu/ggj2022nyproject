﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Button_GotoURL : MonoBehaviour, IPointerDownHandler  {
	public string url = "link";
	

	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.pointerId == -1) {
			ButtonPressed();
		}
	}

	void ButtonPressed(){
		Application.OpenURL(url);
	}
}
