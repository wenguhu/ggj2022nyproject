﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureOffsetLoop : MonoBehaviour {
	[SerializeField]
	Renderer[] renderers;
	[SerializeField]
	Vector2 offsetChangeSpeed = new Vector2(-1, 0);

	[SerializeField]
	bool RandomOffset = true;

	void Start()
	{
		if (RandomOffset)
		{
			foreach (Renderer r in renderers)
			{
				Vector2 offset = r.material.mainTextureOffset;
				offset += new Vector2(Random.Range(0.0000f, offsetChangeSpeed.x * 10), Random.Range(0.0000f, offsetChangeSpeed.y * 10));
				offset.x %= 100000;
				offset.y %= 100000;

				r.material.mainTextureOffset = offset;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		Vector2 offsetChange = Time.deltaTime * offsetChangeSpeed;

		foreach (Renderer r in renderers)
		{
			Vector2 offset = r.material.mainTextureOffset;
			offset += offsetChange;
			offset.x %= 100000;
			offset.y %= 100000;

			r.material.mainTextureOffset = offset;
		}
	}
}
