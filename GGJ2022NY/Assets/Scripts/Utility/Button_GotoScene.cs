﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Button_GotoScene : MonoBehaviour, IPointerDownHandler  {
	public string levelName = "Level Name";
	public float playDelay = 0;
	public Animator anim;
    public bool stopMusicWhenPressed = true;

    private bool pressed = false;
	

	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.pointerId == -1 && !pressed) {
			ButtonPressed();
		}
	}

	void ButtonPressed(){
		//Start Game
		Invoke("StartLoadingScene", playDelay);
		if(MusicManager.instance && stopMusicWhenPressed){
			MusicManager.instance.StopMusicSmooth();
		}
		if(anim){
			anim.SetBool("FadeOut", true);
		}
		pressed = true;
		//Back to the normal timeScale
		Time.timeScale = 1;
	}

	void StartLoadingScene(){
		GlobalSettings.CURRENT_LOADING_SCENE = levelName;
		SceneManager.LoadScene("LoadingState");
	}
}
