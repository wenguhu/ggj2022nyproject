﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class Button_DelegateTrigger : MonoBehaviour, IPointerClickHandler
{
	public delegate void ButtonPressed();
	public ButtonPressed buttonPressed;
	public void OnPointerClick(PointerEventData eventData)
	{
		if (eventData.pointerId == -1)
		{
			buttonPressed.Invoke();
		}
	}
}
