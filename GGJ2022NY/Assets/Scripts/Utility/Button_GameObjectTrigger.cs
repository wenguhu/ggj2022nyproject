﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Button_GameObjectTrigger : MonoBehaviour, IPointerDownHandler
{

	[SerializeField]
	public GameObject triggeredGameObject;

	public void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.pointerId == -1)
		{
			ButtonTriggered(!triggeredGameObject.activeSelf);
		}
	}

	public void ButtonTriggered(bool triggerBool)
	{
		triggeredGameObject.SetActive(triggerBool);
	}
}
