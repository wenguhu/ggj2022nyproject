﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyesBlink : MonoBehaviour {
	[SerializeField]
	SpriteRenderer spriteRenderer;

	[SerializeField]
	Sprite closeEyesSprite;

	[SerializeField]
	float blinkRate = 0.001f;
	[SerializeField]
	float keepClosingTimerTotal = 0.5f;

	//0=None 1=CloseEyes 2=KeepClosing 3=OpenEyes
	int isBlinking = 0;
	Sprite originalEyesSprite;
	float keepClosingTimer = 0;
	// Use this for initialization
	void Start () {
		Reset();
	}

	public void Reset()
	{
		//Reset for face change
		originalEyesSprite = spriteRenderer.sprite;
		isBlinking = 0;
		keepClosingTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (isBlinking == 0)
		{
			if (Random.value < blinkRate)
			{
				isBlinking = 1;
			}
		}
		else if(isBlinking == 1)
		{
			transform.localScale -= new Vector3(0, Time.deltaTime * 20f, 0);
			if(transform.localScale.y <= 0.7f)
			{
				transform.localScale = Vector3.one;
				spriteRenderer.sprite = closeEyesSprite;
				isBlinking = 2;
			}
		}
		else if (isBlinking == 2)
		{
			if (keepClosingTimer > keepClosingTimerTotal)
			{
				keepClosingTimer = 0;
				transform.localScale = new Vector3(1, 0.1f, 1);
				spriteRenderer.sprite = originalEyesSprite;
				isBlinking = 3;
			}
			else
			{
				keepClosingTimer += Time.deltaTime;
			}
		}
		else if (isBlinking == 3)
		{
			transform.localScale += new Vector3(0, Time.deltaTime * 15f, 0);
			if (transform.localScale.y >= 1)
			{
				transform.localScale = Vector3.one;
				isBlinking = 0;
			}
		}
	}
}
