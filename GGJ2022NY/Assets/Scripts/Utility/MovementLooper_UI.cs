﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementLooper_UI : MonoBehaviour
{
	[SerializeField]
	RectTransform content;
	[SerializeField]
	Vector2 moveDistance = new Vector2(-700, 5000);
	[SerializeField]
	float speed = 100;
	// Start is called before the first frame update
	void OnEnable()
    {
		Reset();
	}

    // Update is called once per frame
    void Update()
	{
		content.anchoredPosition += new Vector2(0, speed * Time.deltaTime);
		if (content.anchoredPosition.y > moveDistance.y)
		{
			Reset();
		}
	}

	void Reset()
	{
		content.anchoredPosition = new Vector2(content.anchoredPosition.x, moveDistance.x);
	}
}
