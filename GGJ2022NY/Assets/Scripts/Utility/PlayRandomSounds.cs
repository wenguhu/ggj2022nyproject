﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomSounds : MonoBehaviour
{
	AudioSource aS;
	[SerializeField]
	AudioClip[] audioClipsList;

	[SerializeField]
	bool isRandomPitch = false;

	public float lowPitchRange = .95f;
	public float highPitchRange = 1.05f;
	// Start is called before the first frame update
	void Awake()
	{
		aS = GetComponent<AudioSource>();
		PlayRandomizeSFX();
	}

	public float PlayRandomizeSFX()
	{
		aS.Stop();

		int randomIndex = Random.Range(0, audioClipsList.Length);

		if (isRandomPitch)
		{
			float randomPitch = Random.Range(lowPitchRange, highPitchRange);

			aS.pitch = randomPitch;
		}
		else
		{
			aS.pitch = 1;
		}
		aS.clip = audioClipsList[randomIndex];
		aS.Play();
		return aS.clip.length;
	}
}
