﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AlphaPointerMask : MonoBehaviour, ICanvasRaycastFilter {
	[SerializeField]
	float alphaThreshold = 0.5f;
	protected Image _image;
	// Use this for initialization
	void Start () {
		_image = GetComponent<Image>();

		Texture2D tex = _image.sprite.texture as Texture2D;

		bool isInvalid = false;
		if(tex != null)
		{
			try
			{
				tex.GetPixels32();
			}
			catch(UnityException e)
			{
				Debug.LogError(e.Message);
				isInvalid = true;
			}
		}
		else
		{
			isInvalid = true;
		}
		if (isInvalid)
		{
			Debug.LogError("Need Image component and a readable Texture2D");
		}
	}
	
	public bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
	{
		Vector2 localPoint;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(_image.rectTransform, sp, eventCamera, out localPoint);

		Vector2 pivot = _image.rectTransform.pivot;
		Vector2 normalizedLocal = new Vector2(pivot.x + localPoint.x / _image.rectTransform.rect.width, pivot.y + localPoint.y / _image.rectTransform.rect.height);
		Vector2 uv = new Vector2(
			_image.sprite.rect.x + normalizedLocal.x * _image.sprite.rect.width,
			_image.sprite.rect.y + normalizedLocal.y * _image.sprite.rect.height);

		uv.x /= _image.sprite.texture.width;
		uv.y /= _image.sprite.texture.height;

		//UV are inversed, as 0,0 to be upper right, then going negative towards lower left.
		Color c = _image.sprite.texture.GetPixelBilinear(uv.x, uv.y);

		return c.a > alphaThreshold;
	}
}
