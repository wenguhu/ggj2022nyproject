﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
	[SerializeField]
	Vector3 speedVec = new Vector3(-7, 0, 0);

    // Update is called once per frame
    void Update()
	{
		transform.Translate(speedVec * Time.deltaTime);
	}
}
