﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectShowUp : MonoBehaviour {

	[SerializeField]
	GameObject content;

	[SerializeField]
	float afterSeconds = 2;

	[SerializeField]
	bool isShowingOff = false;

	// Use this for initialization
	void Start()
	{
		if (content != null)
		{
			content.SetActive(isShowingOff);
		}
		else
		{
			content = gameObject;
		}
		Invoke("DisplayContent", afterSeconds);
	}

	void DisplayContent()
	{
		if (content != null)
		{
			content.SetActive(!isShowingOff);
		}
	}
}
