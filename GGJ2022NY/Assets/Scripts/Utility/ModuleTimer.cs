﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleTimer : MonoBehaviour {
	
	[HideInInspector]
	public float timer = 30;
	[HideInInspector]
	public float timerTotal = 30;

	bool isCounting = false;

	public delegate void TimesUp();
	public TimesUp OnTimesUp;

	[SerializeField]
	Image timeDisc;
	[SerializeField]
	SuperTextMesh timerText;

	// Update is called once per frame
	void Update ()
	{
		//Cricket catching check
		if (isCounting)
		{
			if (timer <= 0)
			{
				//Time's up
				timer = 0;
				isCounting = false;
				if(OnTimesUp != null)
				{
					OnTimesUp.Invoke();
				}
			}
			else
			{
				timer -= Time.deltaTime;
			}
			if (timeDisc != null)
			{
				timeDisc.fillAmount = timer / timerTotal;
			}
			if (timerText != null)
			{
				timerText.text = "" + Mathf.RoundToInt(timer) + "s";
			}
		}
	}

	//External triggered. Start timer.
	public void StartTimer(float tTotal)
	{
		timerTotal = tTotal;
		timer = timerTotal;

		isCounting = true;
	}
}
