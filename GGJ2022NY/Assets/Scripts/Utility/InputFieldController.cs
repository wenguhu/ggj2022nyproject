﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldController : MonoBehaviour {
	[SerializeField]
	InputField numberInput;
	// Use this for initialization
	public void ValueChanged_NoMinus (string txt)
	{
		if (txt.Contains("-"))
		{
			txt = txt.Remove(0, 1);
			numberInput.text = txt;
		}
	}
}
