﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class Button_BasicButtonBehavior : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
{
	[SerializeField]
	Image buttonImage;
	[SerializeField]
	Color rollOverColor;
	[SerializeField]
	Color pressedColor;
	[SerializeField]
	Vector2 positionMove = new Vector2(0, -10);
	//Idle:0 / Enter:1 / Press:2
	int pressedTrigger = 0;
	RectTransform rectTrans;
	Vector2 targetPosition;
	Vector2 originalPosition;
	Color originalColor;

	[SerializeField]
	bool buttonHold = true;
	[SerializeField]
	bool pressAgainToDisable = true;
	[SerializeField]
	bool pressedAtStart = false;
	//Has press state
	[SerializeField]
	bool pressStateTriggered = true;
	//For special button that have 2 images, e.g. table size buttons
	[SerializeField]
	bool onlyMoveImageSprite = false;

	[SerializeField]
	float transitionSpeed = 5;

	// Use this for initialization
	void Start () {
		if (onlyMoveImageSprite)
		{
			rectTrans = buttonImage.GetComponent<RectTransform>();
		}
		else
		{
			rectTrans = GetComponent<RectTransform>();
		}
		originalColor = buttonImage.color;
		originalPosition = rectTrans.anchoredPosition;
		targetPosition = originalPosition + positionMove;
		if (pressedAtStart)
		{
			pressedTrigger = 2;
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.pointerId == -1 && pressStateTriggered)
		{
			if(pressedTrigger == 2 && pressAgainToDisable)
			{
				Deselect();
			}
			else
			{
				pressedTrigger = 2;
			}
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (eventData.pointerId == -1 && !buttonHold)
		{
			pressedTrigger = 0;
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (pressedTrigger != 2)
		{
			pressedTrigger = 1;
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (pressedTrigger != 2)
		{
			pressedTrigger = 0;
		}
	}

	public void Deselect()
	{
		pressedTrigger = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if (pressedTrigger == 0)
		{
			buttonImage.color = Color.Lerp(buttonImage.color, originalColor, Time.deltaTime * transitionSpeed / Time.timeScale);
			rectTrans.anchoredPosition = Vector2.Lerp(rectTrans.anchoredPosition, originalPosition, Time.deltaTime * transitionSpeed / Time.timeScale);
		}
		else if (pressedTrigger == 1)
		{
			buttonImage.color = Color.Lerp(buttonImage.color, rollOverColor, Time.deltaTime * transitionSpeed / Time.timeScale);
		}
		else if (pressedTrigger == 2)
		{
			buttonImage.color = Color.Lerp(buttonImage.color, pressedColor, Time.deltaTime * transitionSpeed / Time.timeScale);
			rectTrans.anchoredPosition = Vector2.Lerp(rectTrans.anchoredPosition, targetPosition, Time.deltaTime * transitionSpeed / Time.timeScale);
		}
	}

	void OnDisable()
	{
		if(pressedTrigger == 1)
		{
			pressedTrigger = 0;
		}
	}
}
