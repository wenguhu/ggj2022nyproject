﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LT_ShowUpAnimation : MonoBehaviour {

	[SerializeField]
	private Vector3 startScale = new Vector3(0, 0, 0);
	[SerializeField]
	private Vector3 finalScale = new Vector3(1, 1, 1);
	[SerializeField]
	private float transitionTime = 0.5f;

	// Use this for initialization
	void OnEnable () {
		StartAnimation();
	}
	

	// Update is called once per frame
	public void StartAnimation() {
		transform.localScale = startScale;
		LeanTween.scale(gameObject, finalScale, transitionTime * Time.timeScale).setEase(LeanTweenType.easeOutBack);
	}
}
