﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMaterialColorChanger : MonoBehaviour
{
	[SerializeField]
	bool randomizeEveryElements = false;
	[SerializeField]
	Renderer[] renderers;
	// Use this for initialization
	void OnEnable () {
		Color c = new Color(Random.value, Random.value, Random.value);
		foreach(Renderer r in renderers)
		{
			r.materials[0].color = c;
			if (randomizeEveryElements)
			{
				c = new Color(Random.value, Random.value, Random.value);
			}
		}
	}
}
