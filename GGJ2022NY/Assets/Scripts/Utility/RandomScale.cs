﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScale : MonoBehaviour {
	[SerializeField]
	Vector2 scaleRange = new Vector2(0.5f, 2f);
	// Use this for initialization
	void Start () {
		float scaleValue = Random.Range(scaleRange.x, scaleRange.y);
		transform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);
	}
}
