﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour, IPointerDownHandler  {
	public string levelName = "Level Name";
	public float playDelay = 0;
	public Animator anim;
    public bool stopMusicWhenPressed = true;

    private bool pressed = false;

	void OnGUI(){
		Event e = Event.current;
		//Temporary
		bool gameControllerButtonDown = false;
		for(int i = 0; i < 20; i++){
			if(Input.GetKeyUp("joystick 1 button " + i)){
				gameControllerButtonDown = true;
			}
		}

		if(e.isMouse || e.isKey || gameControllerButtonDown || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
		ButtonPressed();
	}
	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.pointerId == -1 && !pressed) {
			ButtonPressed();
		}
	}

	void ButtonPressed(){
		//Start Game
		Invoke("StartLoadingScene", playDelay);
		if(MusicManager.instance && stopMusicWhenPressed){
			MusicManager.instance.StopMusicSmooth();
		}
		if(anim){
			anim.SetBool("FadeOut", true);
		}
		pressed = true;
	}

	void StartLoadingScene(){
		SceneManager.LoadScene (levelName);
	}
}
