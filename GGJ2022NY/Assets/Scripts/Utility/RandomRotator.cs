﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {
	[SerializeField]
	bool totallyRandom = false;
	// Use this for initialization
	void Start () {
		if (totallyRandom)
		{
			transform.rotation = new Quaternion(Random.value * 360, Random.value * 360, Random.value * 360, Random.value * 360);
		}
		else
		{
			transform.Rotate(Vector3.up, Random.Range(0, 360));
		}
	}
}
