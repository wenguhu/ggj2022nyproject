﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundUnit : MonoBehaviour {
	AudioSource aS;
	[SerializeField]
	AudioClip[] audioClipsList;

	public float lowPitchRange = .95f;
	public float highPitchRange = 1.05f;
	// Use this for initialization
	void Awake () {
		aS = GetComponent<AudioSource>();
	}
	
	//
	public float PlaySingleSFX(int clipID, bool isRandomPitch = true) {
		aS.Stop();
		if (isRandomPitch)
		{
			float randomPitch = Random.Range(lowPitchRange, highPitchRange);

			aS.pitch = randomPitch;
		}
		else
		{
			aS.pitch = 1;
		}
		aS.clip = audioClipsList[clipID];
		aS.Play();
		return aS.clip.length;
	}

	public float PlayRandomizeSFX(int[] clipIDs, bool isRandomPitch = true)
	{
		aS.Stop();
		//Get id from clipIDs
		int randomIndex = clipIDs[Random.Range(0, clipIDs.Length)];

		if (isRandomPitch)
		{
			float randomPitch = Random.Range(lowPitchRange, highPitchRange);

			aS.pitch = randomPitch;
		}
		else
		{
			aS.pitch = 1;
		}
		aS.clip = audioClipsList[randomIndex];
		aS.Play();
		return aS.clip.length;
	}
}
