﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplayer_Game : TextDisplayer
{
	[SerializeField]
	Text[] backs;
	[SerializeField]
	Text resume;
	[SerializeField]
	Text restart;
	[SerializeField]
	Text backToMainMenu;
	[SerializeField]
	Text Quit;
	//[SerializeField]
	//Text apply;



	// Use this for initialization
	public override void LanguageCheck() {
		resume.text = GeneralNote.GetPauseMenuNoteByID(0);
		backToMainMenu.text = GeneralNote.GetPauseMenuNoteByID(1);
		Quit.text = GeneralNote.GetPauseMenuNoteByID(2);
		restart.text = GeneralNote.GetPauseMenuNoteByID(3);

		foreach (Text back in backs)
		{
			back.text = GeneralNote.GetNoteByID(2);
		}

		if (GlobalSettings.CURRENT_LANGUAGE == "CN")
		{
			//startGame.fontSize = 23;
			//setting.fontSize = 23;
			//foreach (Text back in backs)
			//{
			//	back.fontSize = 17;
			//}
		}
		else if (GlobalSettings.CURRENT_LANGUAGE == "EN")
		{
			//startGame.fontSize = 23;
			//setting.fontSize = 23;
			//quit.fontSize = 23;
			//foreach (Text back in backs)
			//{
			//	back.fontSize = 17;
			//}
		}
	}
	
}
