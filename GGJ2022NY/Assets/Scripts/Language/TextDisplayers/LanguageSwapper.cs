﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSwapper : MonoBehaviour {
	public GameObject[] gO_CN;
	public GameObject[] gO_EN;
	// Use this for initialization
	void OnEnable()
	{
		GlobalSettings.TextDisplayerGroupRefresh += LanguageCheck;
		LanguageCheck();
	}

	void OnDisable()
	{
		GlobalSettings.TextDisplayerGroupRefresh -= LanguageCheck;
	}

	void LanguageCheck() {
		if (GlobalSettings.CURRENT_LANGUAGE == "CN")
		{
			foreach(GameObject g in gO_CN)
			{
				g.SetActive(true);
			}
			foreach (GameObject g in gO_EN)
			{
				g.SetActive(false);
			}
		}
		else if (GlobalSettings.CURRENT_LANGUAGE == "EN")
		{
			foreach (GameObject g in gO_CN)
			{
				g.SetActive(false);
			}
			foreach (GameObject g in gO_EN)
			{
				g.SetActive(true);
			}
		}
	}
	
}
