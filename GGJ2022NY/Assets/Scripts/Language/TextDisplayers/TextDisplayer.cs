﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDisplayer : MonoBehaviour {

	void OnEnable()
	{
		GlobalSettings.TextDisplayerGroupRefresh += LanguageCheck;
		LanguageCheck();
	}

	void OnDisable()
	{
		GlobalSettings.TextDisplayerGroupRefresh -= LanguageCheck;
	}

	// Update is called once per frame
	public virtual void LanguageCheck() {
		
	}
}
