﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplayer_Menu : TextDisplayer
{
	[SerializeField]
	Text[] backs;
	[SerializeField]
	Text startGame;
	[SerializeField]
	Text options;
	[SerializeField]
	SuperTextMesh language;
	[SerializeField]
	Text setting;
	//[SerializeField]
	//Text apply;



	// Use this for initialization
	public override void LanguageCheck() {
		startGame.text = GeneralNote.GetNoteByID(0);
		options.text = GeneralNote.GetNoteByID(1);
		language.text = GeneralNote.GetNoteByID(3);
		setting.text = GeneralNote.GetNoteByID(4);

		foreach (Text back in backs)
		{
			back.text = GeneralNote.GetNoteByID(2);
		}

		if (GlobalSettings.CURRENT_LANGUAGE == "CN")
		{
			//startGame.fontSize = 23;
			//setting.fontSize = 23;
			//foreach (Text back in backs)
			//{
			//	back.fontSize = 17;
			//}
		}
		else if (GlobalSettings.CURRENT_LANGUAGE == "EN")
		{
			//startGame.fontSize = 23;
			//setting.fontSize = 23;
			//quit.fontSize = 23;
			//foreach (Text back in backs)
			//{
			//	back.fontSize = 17;
			//}
		}
	}
	
}
