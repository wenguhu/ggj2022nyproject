﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralNote
{
	//CN
	//10=返回
	public static string[] noteGroup_UIProperties = { "Start Game", "Options", "Back", "Language", "Music\n\nSFX" };
	public static string[] noteGroup_UIProperties_EN = { "Start Game", "Options", "Back", "Language", "Music\n\nSFX" };
	public static string[] noteGroup_UI_PauseMenu = { "Resume", "Back to Main Menu", "Quit", "Restart Level", "" };
	public static string[] noteGroup_UI_PauseMenu_EN = { "Resume", "Back to Main Menu", "Quit", "Restart Level", "" };

	// Use this for initialization
	void Start () {
		
	}
	

	public static string GetNoteByID (int id) {
		if (GlobalSettings.CURRENT_LANGUAGE == "CN")
		{
			return noteGroup_UIProperties[id];
		}
		else if (GlobalSettings.CURRENT_LANGUAGE == "EN")
		{
			return noteGroup_UIProperties_EN[id];
		}
		return "";
	}

	public static string GetPauseMenuNoteByID(int id)
	{
		if (GlobalSettings.CURRENT_LANGUAGE == "CN")
		{
			return noteGroup_UI_PauseMenu[id];
		}
		else if (GlobalSettings.CURRENT_LANGUAGE == "EN")
		{
			return noteGroup_UI_PauseMenu_EN[id];
		}
		return "";
	}

	public static string GetSettingLanguageNote(string languageName)
	{
		if (languageName == "CN")
		{
			return "中文";
		}
		else if (languageName == "EN")
		{
			return "English";
		}
		return "";
	}
	
}

