﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
	[SerializeField]
	LocalReference reference;
	GameManager gameManager;

	[SerializeField]
	GameObject pauseMenu;

	[SerializeField]
	Animator[] anim_Layers;

	[SerializeField]
	Slider[] sliders;

	[SerializeField]
	TMP_InputField iF_CodeBox;


	[SerializeField]
	Animator maskAnim;

	string nextGameScene;

	[SerializeField]
	int currentShotID = 0;

	int nextShotID = 0;

	[SerializeField]
	GameObject[] shotList;

	public delegate void OnStartSwitchScene();
	public OnStartSwitchScene onStartSwitchScene;

	Camera mainCamera;
	RectTransform canvasRect;

	public Vector2 ScreenToCanvasPoint(Vector2 object2DPosition)
	{
		//Mouse position to Canvas Pos
		//if(mainCamera == null) Init();
		Vector2 ViewportPosition = mainCamera.ScreenToViewportPoint(object2DPosition);
		Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);

		//Debug: Canvas scaler should be 'Match Height' mode
		Vector2 uiOffset = new Vector2(canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);
		// Set the position and remove the screen offset
		return proportionalPosition - uiOffset;
	}

	// Use this for initialization
	void Start()
	{
		gameManager = reference.GetReference<GameManager>("GameManager");
		mainCamera = reference.GetReference<Transform>("MainCamera").GetComponent<Camera>();
		canvasRect = GetComponent<RectTransform>();
		foreach (Animator anim in anim_Layers)
		{
			anim.speed = 0;
		}

		for(int i = 0; i < shotList.Length; i++)
		{
			if(i == 0)
			{
				shotList[i].SetActive(true);
				sliders[i].gameObject.SetActive(true);
			}
			else
			{
				shotList[i].SetActive(false);
				sliders[i].gameObject.SetActive(false);
			}
		}
	}

	void OnEnable()
	{
	}

	void OnDisable()
	{
	}

	public void OnSlideValueChanged()
	{
		anim_Layers[currentShotID].Play("Layers", -1, sliders[currentShotID].normalizedValue);
	}

	//When Steam Overlay Activated or "Cancel" button is pressed
	void PauseMenuTriggered()
	{
		//TODO: Find a better way to check this
		if (GameManager.gameStatus == GameStatus.Standard)
		{
			PauseMenuStart();
		}
		else
		{
			PauseMenuEnd();
		}
	}

	// Update is called once per frame
	void Update() {
		//**Pause the game & show up pause menu
		if (Input.GetButtonDown("Cancel"))
		{
			PauseMenuTriggered();
		}

		mainCamera.transform.position = -0.0003f * ScreenToCanvasPoint(Input.mousePosition);

	}
	

	//**Pause menu
	public void PauseMenuStart()
	{
		GameManager.gameStatus = GameStatus.Pause;
		//Time.timeScale = 0.01f;
		Time.timeScale = 0;

		pauseMenu.SetActive(true);
	}
	public void PauseMenuEnd()
	{
		GameManager.gameStatus = GameStatus.Standard;
		Time.timeScale = 1;

		pauseMenu.SetActive(false);
	}
	public void QuitGame()
	{
		Application.Quit();
	}
	public void Restart()
	{
		Time.timeScale = 1;
		gameManager.Restart();
	}
	public void Resume()
	{
		PauseMenuEnd();
	}
	public void BackToMainMenu()
	{
		Time.timeScale = 1;
		gameManager.BackToMainMenu();
	}

	public void Button_CodeBoxGo()
	{
		if(iF_CodeBox.text == "17392")
		{
			GameWin();
		}
	}
	public void GameWin()
	{
		//Start Game
		nextGameScene = "WinState";
		Invoke("GotoScene", 3);
		if (MusicManager.instance)
		{
			MusicManager.instance.StopMusicSmooth();
		}
		if (maskAnim)
		{
			maskAnim.SetBool("FadeOut", true);
		}
		//Back to the normal timeScale
		Time.timeScale = 1;
	}

	//***Game starts
	void GotoScene()
	{
		GlobalSettings.CURRENT_LOADING_SCENE = nextGameScene;
		SceneManager.LoadScene("LoadingState");
	}

	public void SwitchSceneInAnimation()
	{
		if(nextShotID != -1)
		{
			shotList[currentShotID].SetActive(false);
			sliders[currentShotID].gameObject.SetActive(false);
			currentShotID = nextShotID;
			shotList[currentShotID].SetActive(true);
			sliders[currentShotID].gameObject.SetActive(true);
			nextShotID = -1;
			OnSlideValueChanged();
		}
	}

	public void GotoNextShot()
	{
		nextShotID = (currentShotID + 1) % shotList.Length;
		if (onStartSwitchScene != null) onStartSwitchScene.Invoke();
	}
	public void GotoPrevShot()
	{
		nextShotID = currentShotID;
		nextShotID--;
		if(nextShotID < 0)
		{
			nextShotID = shotList.Length - 1;
		}
		if (onStartSwitchScene != null) onStartSwitchScene.Invoke();
	}
}
