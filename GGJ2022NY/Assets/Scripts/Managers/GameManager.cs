﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStatus { Standard, Pause }
public class GameManager : MonoBehaviour
{
	public static GameStatus gameStatus = GameStatus.Standard;
	public delegate void GameOverEventCaller();
	public GameOverEventCaller GameOverEvent;

	[SerializeField]
	LocalReference reference;
	UIManager uiManager;
	SoundUnit soundUnit;

	//BGM
	[SerializeField]
	bool tempMusicToggle = true;
	[SerializeField]
	AudioClip bgm;
	[SerializeField]
	float bgmLoopingPoint = 82;

	[SerializeField]
	Animator maskAnim;

	[SerializeField]
	string nextScene;

	[HideInInspector]
	public bool isGameOver = false;

	// Use this for initialization
	void Start ()
	{
		//Init
		StaticInit();
		
		uiManager = reference.GetReference<UIManager>("UIManager");

		//Time.timeScale = 2f;


		soundUnit = GetComponent<SoundUnit>();
		if (tempMusicToggle)
		{
			MusicManager.instance.PlayMusic(bgm, bgmLoopingPoint);
		}
	}

	void StaticInit()
	{
		GameManager.gameStatus = GameStatus.Standard;
		Time.timeScale = 1;
	}

	void Update()
	{
		if (!isGameOver)
		{

		}
	}


	public void GameOver()
	{
		isGameOver = true;

		MusicManager.instance.StopMusicSmooth();
		//Play death animation
		Invoke("FadeOut", 3);
		
	}

	void FadeOut()
	{
		maskAnim.SetTrigger("FadeOut");
		Invoke("GotoScene", 2);
	}


	void GotoScene()
	{
		GlobalSettings.CURRENT_LOADING_SCENE = nextScene;
		SceneManager.LoadScene("LoadingState");
	}

	public void Restart()
	{
		nextScene = SceneManager.GetActiveScene().name;
		FadeOut();
	}
	public void BackToMainMenu()
	{
		nextScene = "MenuState";
		FadeOut();
	}
}
