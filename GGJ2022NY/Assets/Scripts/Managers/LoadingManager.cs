﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
	[SerializeField]
	Text text_Loading;
	[SerializeField]
	RectTransform bar_Loading;
	[SerializeField]
	int bar_Width = 1200;
	[SerializeField]
	int bar_Height = 30;
	[SerializeField]
	Animator blackMask;

	[SerializeField]
	float timerLoading = 0;
	[SerializeField]
	float timerLoadingTotal = 3;

	int displayProgress = 0;
	int toProgress = 0;
	AsyncOperation op;


	// Use this for initialization
	void Start()
	{
		StartCoroutine(StartLoading(GlobalSettings.CURRENT_LOADING_SCENE));
	}

	void Update()
	{
		SetLoadingBar(displayProgress);

		if (timerLoading > timerLoadingTotal)
		{
			//Wait 2.1 seconds for the fade in blackmask
			if (displayProgress == 100)
			{
				blackMask.SetTrigger("FadeOut");

				Invoke("FinishLoading", 2.1f);
			}
		}
		else
		{
			timerLoading += Time.deltaTime;
		}
	}

	private void FinishLoading()
	{
		op.allowSceneActivation = true;
	}

	// Update is called once per frame
	private IEnumerator StartLoading(string sceneName)
	{
		//bgAnimation.enabled = true;
		yield return new WaitForSeconds(0.5f);

		op = SceneManager.LoadSceneAsync(sceneName);
		//Wait until 90%
		op.allowSceneActivation = false;
		bar_Loading.gameObject.SetActive(true);
		while (op.progress < 0.9f)
		{
			//90%
			toProgress = Mathf.RoundToInt(op.progress * 100);
			while (displayProgress < toProgress)
			{
				++displayProgress;
				yield return new WaitForEndOfFrame();
			}
			yield return new WaitForEndOfFrame();
		}
		
		//visitorDB.CreateInitVisitors();
		toProgress = 100;
		while (displayProgress < toProgress)
		{
			displayProgress += 10;
			if(displayProgress > toProgress)
			{
				displayProgress = toProgress;
			}
			//SetLoadingBar(displayProgress);
			yield return new WaitForEndOfFrame();
		}
	}

	private void SetLoadingBar(int progress)
	{
		text_Loading.text = progress + "%";
		bar_Loading.sizeDelta = BarRatioCalculation(progress, 100);
	}

	Vector2 BarRatioCalculation(float numerator, float denominator)
	{
		float vX = Mathf.Max(0, bar_Width * (numerator / denominator));
		Vector2 v = new Vector2(vX, bar_Height);
		return v;
	}
}
