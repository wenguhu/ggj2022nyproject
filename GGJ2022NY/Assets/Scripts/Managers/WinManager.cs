﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class WinManager : MonoBehaviour
{
	[SerializeField]
	bool tempMusicToggle = true;
	[SerializeField]
	AudioClip bgm;
	[SerializeField]
	float bgmLoopingPoint = 30;

	[SerializeField]
	Animator maskAnim;
	[SerializeField]
	string nextScene = "MenuState";
	


	// Start is called before the first frame update
	void Start()
	{
		GlobalSettings.Load();

		if (tempMusicToggle)
		{
			MusicManager.instance.PlayMusic(bgm, bgmLoopingPoint);
		}


	}

	public void BackToMenu()
	{
		//Start Game
		nextScene = "MenuState";
		Invoke("GotoScene", 3);
		if (MusicManager.instance)
		{
			MusicManager.instance.StopMusicSmooth();
		}
		if (maskAnim)
		{
			maskAnim.SetBool("FadeOut", true);
		}
		//Back to the normal timeScale
		Time.timeScale = 1;
	}

	//***Game starts
	void GotoScene()
	{
		GlobalSettings.CURRENT_LOADING_SCENE = nextScene;
		SceneManager.LoadScene("LoadingState");
	}

}
