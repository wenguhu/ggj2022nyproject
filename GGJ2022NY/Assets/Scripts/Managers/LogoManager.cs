﻿using UnityEngine;
using System.Collections;

public class LogoManager : MonoBehaviour
{
	public AudioClip bgm;
	public float bgmLoopTiming;

	void Start()
	{
		PlayMusic();


		//Read current language
		//string systemLanguage = PlayerPrefs.GetString("SystemLanguage", "None");
		//if (systemLanguage == "None")
		//{
		//	if (Application.systemLanguage == SystemLanguage.Chinese || Application.systemLanguage == SystemLanguage.ChineseSimplified || Application.systemLanguage == SystemLanguage.ChineseTraditional)
		//	{
		//		systemLanguage = "CN";
		//	}
		//	else if (Application.systemLanguage == SystemLanguage.English)
		//	{
		//		systemLanguage = "EN";
		//	}
		//	else
		//	{
		//		systemLanguage = "EN";
		//	}
		//	GlobalSettings.CURRENT_LANGUAGE = systemLanguage;
		//	PlayerPrefs.SetString("SystemLanguage", systemLanguage);
		//}
		//else
		//{
		//	GlobalSettings.CURRENT_LANGUAGE = systemLanguage;
		//}
	}

	void OnGUI()
	{
		Event e = Event.current;

		if (e.Equals(Event.KeyboardEvent("[esc]")))
		{
			Application.Quit();
		}
	}

	void PlayMusic()
	{
		MusicManager.instance.PlayMusic(bgm, bgmLoopTiming);
	}
}
