using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeManager : MonoBehaviour
{
    [SerializeField]
    LocalReference reference;
    UIManager uiManager;

    int currentCodeID = 0;
    [SerializeField]
    SpriteRenderer[] codeNumSlots;
    [SerializeField]
    Sprite[] codeNumSp;

    int[] correctCode = { 3, 8, 9, 9 };

    SoundUnit soundUnit;

    // Start is called before the first frame update
    void Start()
    {
        uiManager = reference.GetReference<UIManager>("UIManager");
        soundUnit = GetComponent<SoundUnit>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyUp(KeyCode.Alpha0))
		{
            codeNumSlots[currentCodeID].sprite = codeNumSp[0];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[1];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[2];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[3];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[4];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[5];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[6];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[7];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[8];
            NextCodeID();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            codeNumSlots[currentCodeID].sprite = codeNumSp[9];
            NextCodeID();
        }

    }

    void NextCodeID()
	{
        soundUnit.PlaySingleSFX(0);
        currentCodeID = (currentCodeID + 1) % codeNumSlots.Length;
        if (currentCodeID == 0)
		{
            //Loop ended, check if the code is correct
            bool codeCheck = true;
            for(int i = 0; i < correctCode.Length; i++)
            {
                if (codeNumSlots[i].sprite != codeNumSp[correctCode[i]])
				{
                    codeCheck = false;
                    break;
                }
            }

			if (codeCheck)
			{
                //Win game
                uiManager.GameWin();
            }
		}
    }
}
