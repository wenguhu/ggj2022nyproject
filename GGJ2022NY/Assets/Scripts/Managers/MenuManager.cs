﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuManager : MonoBehaviour
{
	[SerializeField]
	bool tempMusicToggle = true;
	[SerializeField]
	AudioClip bgm;
	[SerializeField]
	float bgmLoopingPoint = 30;

	[SerializeField]
	Animator maskAnim;
	[SerializeField]
	string nextScene = "GameState";
	

	//***Options
	[SerializeField]
	Text language_Text;

	//For option applying
	float mixerVolume_Music = -10;
	float mixerVolume_SFX = 0;

	//Volume change
	[SerializeField]
	AudioMixer masterMixer;
	[SerializeField]
	Slider slider_Music;
	[SerializeField]
	Slider slider_SFX;

	// Start is called before the first frame update
	void Start()
	{
		GlobalSettings.Load();

		if (tempMusicToggle)
		{
			MusicManager.instance.PlayMusic(bgm, bgmLoopingPoint);
		}


		//Setup volume
		mixerVolume_Music = PlayerPrefs.GetFloat("mixerVolume_Music", -10);
		mixerVolume_SFX = PlayerPrefs.GetFloat("mixerVolume_SFX", 0);
		Options_Init();
		language_Text.text = GeneralNote.GetSettingLanguageNote(GlobalSettings.CURRENT_LANGUAGE);
		
	}

	public void GameStart()
	{
		//Start Game
		nextScene = "GameState";
		Invoke("GotoScene", 3);
		if (MusicManager.instance)
		{
			MusicManager.instance.StopMusicSmooth();
		}
		if (maskAnim)
		{
			maskAnim.SetBool("FadeOut", true);
		}
		//Back to the normal timeScale
		Time.timeScale = 1;
	}

	//***Game starts
	void GotoScene()
	{
		GlobalSettings.CURRENT_LOADING_SCENE = nextScene;
		SceneManager.LoadScene("LoadingState");
	}

	//***Options
	public void Options_LanguageButton()
	{
		string languageSwitchForReloading = GlobalSettings.CURRENT_LANGUAGE;
		if (languageSwitchForReloading == "EN")
		{
			languageSwitchForReloading = "CN";
		}
		else if (languageSwitchForReloading == "CN")
		{
			languageSwitchForReloading = "EN";
		}
		else
		{
			languageSwitchForReloading = "EN";
		}
		language_Text.text = GeneralNote.GetSettingLanguageNote(languageSwitchForReloading);
		PlayerPrefs.SetString("SystemLanguage", languageSwitchForReloading);
		GlobalSettings.CURRENT_LANGUAGE = languageSwitchForReloading;
		GlobalSettings.TextDisplayerGroupRefresh.Invoke();
	}

	public void Options_Init()
	{
		//Back to the last configuration
		masterMixer.SetFloat("sfxVol", mixerVolume_SFX);
		masterMixer.SetFloat("bgmVol", mixerVolume_Music);
		slider_SFX.value = 1 / ((mixerVolume_SFX - 9f) * -0.1f) - 0.15f;
		slider_Music.value = 1 / ((mixerVolume_Music - 9f) * -0.1f) - 0.15f;
	}

	public void Options_Back()
	{
		float f;
		masterMixer.GetFloat("sfxVol", out f);
		mixerVolume_SFX = f;
		masterMixer.GetFloat("bgmVol", out f);
		mixerVolume_Music = f;
		PlayerPrefs.SetFloat("mixerVolume_Music", mixerVolume_Music);
		PlayerPrefs.SetFloat("mixerVolume_SFX", mixerVolume_SFX);

		//QualitySettings.SetQualityLevel(QualitySettings.names.Length - qualitySetting - 1, true);
	}

	public void VolumeChanged()
	{
		masterMixer.SetFloat("sfxVol", -10 / (slider_SFX.value + 0.15f) + 9f);
		masterMixer.SetFloat("bgmVol", -10 / (slider_Music.value + 0.15f) + 9f);
	}

	public void Quit()
	{
		Application.Quit();
	}
}
