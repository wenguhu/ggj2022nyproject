﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ES3Types;

public class GlobalSettings {

	//Language
	public static string CURRENT_LANGUAGE = "EN";

	public delegate void TextDisplayerGroup();
	public static TextDisplayerGroup TextDisplayerGroupRefresh;

	public static string CURRENT_LOADING_SCENE = "";


	//***Global data
	//Savedata Version
	public static float CURRENT_SAVEDATA_VER = 0.1f;
	//Stars
	//public static bool[] stars_Zombie = { false, false, false };

	//Store star currency
	//public static int store_CurrentStars = 0;
	//public static int store_Currency = 0;

	//static ES3Settings es3Settings = new ES3Settings();
	//es3Settings.directory = ES3.Directory.DataPath;

	public static Dictionary<string, bool> unlocks = new Dictionary<string, bool>();

	public static void Load()
	{
		//***Language setting is loaded by LogoManager

		//Savedata version control
		if (CURRENT_SAVEDATA_VER > ES3.Load<float>("saveData_Ver", "savedata/global.cc", 0))
		{
			//unlocks.Add("Character1", true);
			//New savedata version, clear old savedata
			ES3.DeleteFile("savedata/global.cc");
			//ES3.DeleteFile("savedata/leaderboard.tcs");
			ES3.Save<float>("saveData_Ver", CURRENT_SAVEDATA_VER, "savedata/global.cc");
			//ES3.Save<float>("saveData_Ver", CURRENT_SAVEDATA_VER, "savedata/leaderboard.tcs");
			//ES3.Save<Dictionary<string, bool>>("unlocks", unlocks, "savedata/global.cc");
			Debug.Log("Savedata Reset");
		}
		else
		{
			//Load global data
			//stars_Zombie = ES3.Load<bool[]>("stars_Zombie", "savedata/stars.tcs", new bool[] { false, false, false });
			//store_CurrentStars = ES3.Load<int>("store_CurrentStars", "savedata/global.tcs", 0);
			//store_Currency = ES3.Load<int>("store_Currency", "savedata/global.cc", 0);
			//unlocks = ES3.Load<Dictionary<string, bool>>("unlocks", "savedata/global.cc");
		}
		//store_Currency += 9999;



		//Read current language
		//string systemLanguage = PlayerPrefs.GetString("SystemLanguage", "None");
		//if (systemLanguage == "None")
		//{
		//	if (Application.systemLanguage == SystemLanguage.Chinese || Application.systemLanguage == SystemLanguage.ChineseSimplified || Application.systemLanguage == SystemLanguage.ChineseTraditional)
		//	{
		//		systemLanguage = "CN";
		//	}
		//	else if (Application.systemLanguage == SystemLanguage.English)
		//	{
		//		systemLanguage = "EN";
		//	}
		//	else
		//	{
		//		systemLanguage = "EN";
		//	}
		//	GlobalSettings.CURRENT_LANGUAGE = systemLanguage;
		//	PlayerPrefs.SetString("SystemLanguage", systemLanguage);
		//}
		//else
		//{
		//	GlobalSettings.CURRENT_LANGUAGE = systemLanguage;
		//}
	}

	public static void Save()
	{
		//Save global data
		//ES3.Save<bool[]>("stars_Zombie", stars_Zombie, "savedata/stars.tcs");
		//ES3.Save<int>("store_CurrentStars", store_CurrentStars, "savedata/global.tcs");
		//ES3.Save<int>("store_Currency", store_Currency, "savedata/global.cc");
		//ES3.Save<Dictionary<string, bool>>("unlocks", unlocks, "savedata/global.cc");
	}
}
